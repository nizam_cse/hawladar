import React from 'react'

const routes = [
  {
    path: '/',
    exact: true,
    component: React.lazy(() => import('../pages/Home'))
  },
  {
    path: '/categories',
    exact: true,
    component: React.lazy(() => import('../pages/category/List'))
  },
  {
    path: '/categories/create',
    exact: true,
    component: React.lazy(() => import('../pages/category/Form'))
  },
  {
    path: '/categories/update/:id',
    exact: true,
    component: React.lazy(() => import('../pages/category/Form'))
  }
]

export default routes
