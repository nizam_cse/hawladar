import React from 'react'
import { Switch, Route } from 'react-router-dom'

export const RouteWithSubRoutes = (route) => {
  return (
    <Route
      path={route.path}
      render={() => {
        return (
          <route.component routes={route.routes} subRoutes={route.routes} />
        )
      }}
    />
  )
}

const routeConfig = (routes) => {
  if (!routes) return <div />
  return (
    <Switch>
      {routes.map((route, i) => (
        <RouteWithSubRoutes key={i} {...route} />
      ))}
    </Switch>
  )
}

export default routeConfig
