import React, { useContext } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Alert from '@material-ui/lab/Alert'
import { NotificationContext } from '../context/NotificationContext'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2)
    }
  }
}))

const ActionAlerts = () => {
  const { notification, setNotificationData } = useContext(NotificationContext)
  const onCloseAlert = () => {
    setNotificationData({ severity: 'success', text: '', display: false })
  }
  const classes = useStyles()
  if (notification.display)
    return (
      <div className={classes.root}>
        <Alert severity={notification.severity} onClose={onCloseAlert}>
          {notification.text}
        </Alert>
      </div>
    )
  else return null
}

export default ActionAlerts
