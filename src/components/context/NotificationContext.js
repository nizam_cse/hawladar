import React, { createContext, useState } from 'react'
import { node } from 'prop-types'
export const NotificationContext = createContext()

const NotificationContextProvider = (props) => {
  const [notification, setNotification] = useState({
    severity: 'success',
    text: '',
    display: false
  })
  const setNotificationData = (notification) => {
    setNotification({ notification, ...notification })
  }

  return (
    <NotificationContext.Provider value={{ notification, setNotificationData }}>
      {props.children}
    </NotificationContext.Provider>
  )
}
NotificationContextProvider.propTypes = {
  children: node
}
export default NotificationContextProvider
