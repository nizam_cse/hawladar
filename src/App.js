import React, { Suspense } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import routes from './components/routes/routes'
import routeConfig from './components/routes'
import LayoutAlpha from './components/layout/LayoutAlpha'
function App() {
  return (
    <Router>
      <Suspense fallback={'Loading Studycamp'}>
        <LayoutAlpha routes={routes} routeConfig={routeConfig} />
      </Suspense>
    </Router>
  )
}

export default App
